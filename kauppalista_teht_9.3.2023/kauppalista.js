const list = document.querySelector('ul')
const input = document.querySelector('input')
const button = document.querySelector('button')

button.onclick = function () {
    let newitem = input.value;
    input.value = '';

    let itemList = document.createElement('li')
    let itemText = document.createElement('span')
    let itemBtm = document.createElement('button')

    itemList.appendChild(itemText);
    itemText.textContent = newitem;

    itemList.appendChild(itemBtm);
    itemBtm.textContent = 'poista';

    list.appendChild(itemList);

    itemBtm.onclick = function () {
        list.removeChild(itemList);
    }

    input.focus();
}